package ir.syborg.app.dooz;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;


public class ActivityDooz extends Activity {

    public ImageView[] imgList   = new ImageView[9];
    public boolean     playerOne = true;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        imgList[0] = (ImageView) findViewById(R.id.img00);
        imgList[1] = (ImageView) findViewById(R.id.img01);
        imgList[2] = (ImageView) findViewById(R.id.img02);
        imgList[3] = (ImageView) findViewById(R.id.img10);
        imgList[4] = (ImageView) findViewById(R.id.img11);
        imgList[5] = (ImageView) findViewById(R.id.img12);
        imgList[6] = (ImageView) findViewById(R.id.img20);
        imgList[7] = (ImageView) findViewById(R.id.img21);
        imgList[8] = (ImageView) findViewById(R.id.img22);

        OnClickListener click = new OnClickListener() {

            @Override
            public void onClick(View view) {
                ImageView inputView = (ImageView) view;

                if (playerOne) {
                    inputView.setImageResource(R.drawable.one);
                } else {
                    inputView.setImageResource(R.drawable.two);
                }
                playerOne = !playerOne;

            }
        };

        for (int i = 0; i < 9; i++) {
            imgList[i].setOnClickListener(click);
        }

    }
}